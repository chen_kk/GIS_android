package com.chen.arcgisandroid.adapter;

import android.content.Context;
import android.view.View;

import com.chen.arcgisandroid.R;
import com.chen.arcgisandroid.base.BannerBean;

/**
 * Created by Administrator on 2017/10/9.
 */

public class BannerAdapter extends BannerBaseAdapter<BannerBean> {

    public BannerAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.item_banner;
    }

    @Override
    protected void convert(View convertView, BannerBean data) {
        setImage(R.id.pageImage, data.imageRes);
        setText(R.id.pageText, data.title);
    }
}
