package com.chen.arcgisandroid.base;

import android.support.annotation.IntegerRes;

/**
 * Created by Administrator on 2017/10/9.
 */

public class BannerBean {
    public String imageUrl;
    public String title;
    public
    @IntegerRes
    int imageRes;

    public BannerBean(int imageRes, String title) {
        this.imageRes = imageRes;
        this.title = title;
    }
}
