package com.chen.arcgisandroid.activity;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chen.arcgisandroid.R;
import com.chen.arcgisandroid.adapter.BannerAdapter;
import com.chen.arcgisandroid.adapter.BannerBaseAdapter;
import com.chen.arcgisandroid.base.BannerBean;
import com.chen.arcgisandroid.base.BaseActivity;
import com.chen.arcgisandroid.entity.MenuItem;
import com.chen.arcgisandroid.widget.BannerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/9/27.
 */

public class HomeActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.banner)
    BannerView mBanner;
    @BindView(R.id.rv_home_menu)
    RecyclerView mHomeMenu;


    private BannerAdapter mAdapter;
    int[] images = {R.mipmap.timg1, R.mipmap.timg2, R.mipmap.timg3, R.mipmap.timg4};
    int offset = ConvertUtils.dp2px(0.8f);

    int[] icons = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5, R.drawable.img6, R.drawable.img7, R.drawable.img8};

    private int spanCount = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
        initBanner();
        initHomeMenu();
    }

    protected void initHomeMenu() {
        String[] menuNames = getResources().getStringArray(R.array.menu_names);
        List<MenuItem> items = new ArrayList<>();
        for (int i = 0; i < menuNames.length; i++) {
            MenuItem item = new MenuItem();
            item.setIconRes(icons[i]);
            item.setItemName(menuNames[i]);
            items.add(item);
        }
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, spanCount);
        //调用以下方法让RecyclerView的第一个条目仅为1列
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                //如果位置是0，那么这个条目将占用SpanCount()这么多的列数，再此也就是3
                //而如果不是0，则说明不是Header，就占用1列即可
                //return adapter.isHeader(position) ? layoutManager.getSpanCount() : 1;
                return 0;
            }
        });
        mHomeMenu.setLayoutManager(layoutManager);
        BaseQuickAdapter adapter = new BaseQuickAdapter<MenuItem, BaseViewHolder>(R.layout.item_home_menu, items) {

            @Override
            protected void convert(BaseViewHolder helper, MenuItem item) {
                helper.setText(R.id.tv_home_menu_name, item.getItemName());
                helper.setImageResource(R.id.iv_home_menu_icon, item.getIconRes());
            }
        };
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(mContext, MainActivity.class));
                        break;
                }
            }
        });

        adapter.bindToRecyclerView(mHomeMenu);
        mHomeMenu.addItemDecoration(new MyItemDecoration());
    }

    @Override
    protected void initViews() {
        super.initViews();
        initToolBar(toolbar, true, null);
        toolbar.findViewById(R.id.ib_right_image_button).setVisibility(View.GONE);
        tvTitle.setText("首页");
    }

    private void initBanner() {
        List<BannerBean> beanList = new ArrayList<>();
        for (int i = 0; i < images.length; i++) {
            beanList.add(new BannerBean(images[i], "图片" + i));
        }
        mAdapter = new BannerAdapter(mContext);
        mBanner.setAdapter(mAdapter);
        mAdapter.setOnPageTouchListener(new BannerBaseAdapter.OnPageTouchListener<BannerBean>() {
            @Override
            public void onPageClick(int position, BannerBean bannerBean) {
                // 页面点击
                ToastUtils.showShort(bannerBean.title);
            }

            @Override
            public void onPageDown() {
                // 按下，可以停止轮播
                //   Toast.makeText(BannerActivity.this, "按下", Toast.LENGTH_SHORT).show();
                mBanner.stopAutoScroll();
            }

            @Override
            public void onPageUp() {
                // 抬起开始轮播
                //  Toast.makeText(BannerActivity.this, "抬起", Toast.LENGTH_SHORT).show();
                mBanner.startAutoScroll();
            }
        });
        mAdapter.setData(beanList);
    }

    /**
     * recycleview间隔线
     */
    class MyItemDecoration extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView
                .State state) {
            super.getItemOffsets(outRect, view, parent, state);
            super.getItemOffsets(outRect, view, parent, state);
            int childLayoutPosition = parent.getChildLayoutPosition(view);
            if (childLayoutPosition % spanCount != 0) {
                outRect.right = offset / 2;
                outRect.bottom = offset / 2;
            } else {
                outRect.left = offset / 2;
                outRect.right = offset / 2;
                outRect.bottom = offset / 2;
            }

        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            //始化一个Paint
            Paint paint = new Paint();
//                paint.setColor(Color.parseColor("#B8B8B8"));
//            paint.setColor(Color.parseColor("#D8D8D8"));
            paint.setColor(getResources().getColor(R.color.decoration_color));
            paint.setStrokeWidth(offset);

            //获得RecyclerView中条目数量
            int childCount = parent.getChildCount();

            //遍历
            for (int i = 0; i < childCount; i++) {

                //获得子View，也就是一个条目的View，准备给他画上边框
                View childView = parent.getChildAt(i);

                //先获得子View的长宽，以及在屏幕上的位置
                float x = childView.getX();
                float y = childView.getY();
                int width = childView.getWidth();
                int height = childView.getHeight();

                if (i % spanCount == 2) {
                    if (i < spanCount) {
                        //h top
                        c.drawLine(x, y, x + width, y, paint);
                    }
                    //v right
                    c.drawLine(x + width, y, x + width, y + height, paint);
                    //h bottom
                    c.drawLine(x, y + height, x + width, y + height, paint);
                    continue;
                } else {
                    if (i < spanCount) {
                        //h top
                        c.drawLine(x, y, x + width, y, paint);
                    }
                    if (i % spanCount == 0) {
                        //v left
                        c.drawLine(x, y, x, y + height, paint);
                    }
                    c.drawLine(x + width, y, x + width, y + height, paint);
                    //h bottom
                    c.drawLine(x, y + height, x + width, y + height, paint);
                    continue;
                }
//                    //根据这些点画条目的四周的线 h:水平 v:垂直
//                    //h top
//                    c.drawLine(x, y, x + width, y, paint);
//                    //v left
//                    c.drawLine(x, y, x, y + height, paint);
//                    //v right
//                    c.drawLine(x + width, y, x + width, y + height, paint);
//                    //h bottom
//                    c.drawLine(x, y + height, x + width, y + height, paint);
            }
            super.onDraw(c, parent, state);
            super.onDraw(c, parent, state);
        }
    }
}
