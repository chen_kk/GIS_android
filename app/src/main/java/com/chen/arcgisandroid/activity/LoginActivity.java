package com.chen.arcgisandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.chen.arcgisandroid.R;
import com.chen.arcgisandroid.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/9/27.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.btn_login)
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }
    @OnClick(R.id.btn_login)
    void login(){
        startActivity(new Intent(mContext,HomeActivity.class));
        finish();
    }

}
