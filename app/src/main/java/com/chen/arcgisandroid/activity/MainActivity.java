package com.chen.arcgisandroid.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.chen.arcgisandroid.R;
import com.chen.arcgisandroid.base.BaseActivity;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import pub.devrel.easypermissions.AppSettingsDialog;

public class MainActivity extends BaseActivity {
    @BindView(R.id.mp_gis_map)
    MapView mMapView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ib_amplify)
    ImageButton ibAmplify;
    @BindView(R.id.ib_reduce)
    ImageButton ibReduce;
    @BindView(R.id.ib_translation)
    ImageButton ibTranslation;
    @BindView(R.id.ib_backspace)
    ImageButton ibBackspace;
    @BindView(R.id.ib_compass)
    ImageButton ibCompass;
    @BindView(R.id.dl_drawerlayout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.rg_tools)
    LinearLayout rgTools;
    @BindView(R.id.rb_lcal)
    CheckBox rbLcal;
    @BindView(R.id.rb_area)
    CheckBox rbArea;
    @BindView(R.id.rb_space_board)
    CheckBox rbSpace;
    @BindView(R.id.rb_layer)
    CheckBox rbLayer;



    private static final int LOCATION_REQUEST_CODE = 0x1;
    private String[] needPermission = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    LocationDisplay locationDisplay;
    private List<CheckBox> checkBoxes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initGisMap();

    }

    private void initGisMap() {
        ArcGISMap map = new ArcGISMap(Basemap.Type.TOPOGRAPHIC, 34.056295, -117.195800, 16);
        locationDisplay = mMapView.getLocationDisplay();
        mMapView.setMap(map);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.pause();
    }

    @Override
    protected void permissionsGranted() {
        super.permissionsGranted();
        gisLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission(needPermission, getResources().getString(R.string.need_permission_request), LOCATION_REQUEST_CODE);
        mMapView.resume();
    }

    @Override
    protected void initViews() {
        super.initViews();
        initToolBar(toolbar, true, null);
        tvTitle.setText("test");
        //禁止手势滑动
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //打开手势滑动
        // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        checkBoxes.add(rbLcal);
        checkBoxes.add(rbArea);
        checkBoxes.add(rbSpace);
        checkBoxes.add(rbLayer);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        ToastUtils.showShort("权限申请成功");
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        new AppSettingsDialog.Builder(MainActivity.this, getResources().getString(R.string.location_permission_point_out))
                .setTitle(getResources().getString(R.string.warning))
                .setPositiveButton(getResources().getString(R.string.open_now))
                .setNegativeButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).build()
                .show();
    }

    /**
     * 定位
     */
    private void gisLocation() {
        locationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
        locationDisplay.startAsync();
    }

    //放大地图
    @OnClick(R.id.ib_amplify)
    void mapAmplify() {
        double mScale = mMapView.getMapScale();
        mMapView.setViewpointScaleAsync(mScale * 0.5);

    }

    //缩小地图
    @OnClick(R.id.ib_reduce)
    void mapReduce() {
        double mScale = mMapView.getMapScale();
        mMapView.setViewpointScaleAsync(mScale * 2);
    }

    //平移地图
    @OnClick(R.id.ib_translation)
    void mapTranslation() {
        Point point = locationDisplay.getMapLocation();
        mMapView.setViewpointCenterAsync(point);
    }

    //回退
    @OnClick(R.id.ib_backspace)
    void mapBackspace() {

    }

    //指北针
    @OnClick(R.id.ib_compass)
    void mapCompass() {
        mMapView.setViewpointRotationAsync(0);
    }

    @OnClick(R.id.ib_right_image_button)
    void toggleDrawerLayout() {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
        }
    }
    @OnCheckedChanged(R.id.rb_lcal)
    void lcal(boolean isChecked){
        if(isChecked)
        changeCheck(rbLcal.getId());
    }
    @OnCheckedChanged(R.id.rb_area)
    void area(boolean isChecked){
        if(isChecked)
            changeCheck(rbArea.getId());
    }
    @OnCheckedChanged(R.id.rb_space_board)
    void space(boolean isChecked){
        if(isChecked)
            changeCheck(rbSpace.getId());
    }
    @OnCheckedChanged(R.id.rb_layer)
    void layer(boolean isChecked){
        if(isChecked)
            changeCheck(rbLayer.getId());
    }
    private void changeCheck(int viewId){
        for (CheckBox checkBox : checkBoxes) {
            if(checkBox.getId()!=viewId){
                checkBox.setChecked(false);
            }
        }
    }
}
