package com.chen.arcgisandroid.entity;

/**
 * Created by Administrator on 2017/9/29.
 */

public class MenuItem {
    private int iconRes;
    private String imgUrl;
    private String itemName;
    private int id;

    public MenuItem() {
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
